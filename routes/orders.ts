import { Router, Request, Response } from 'express'
import storage from 'node-persist'
import Controller from '../controllers/Controller'

const router = Router()
const controller = new Controller

router.get('/', async (req: Request, res: Response) =>{ 
  controller.GetOrders(req,res)
})

router.get('/:id', async (req: Request, res: Response) => {
  controller.GetOneOrders(req,res);
})

router.post('/', async (req: Request, res: Response) => {
  controller.AddOrders(req, res)
})

export default router