import express from 'express'
import path from 'path'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import storage from 'node-persist'
import indexRouter from './routes/index'
import ordersRouter from './routes/orders'
import { orders } from './data/_orders'

export default class App{
  private app : express.Application
  
    constructor(){
      this.app = express()
      //this.app.set('port', port)
      this.registerMiddleware()
      this.registerRoutes()
      this.initDefaultData()   

    }

    getApp(){
        return this.app;
    }

    private registerMiddleware(){
      this.app.use(logger('dev'))
      this.app.use(express.json())
      this.app.use(cookieParser())
      
    }
    private registerRoutes(){
      this.app.use('/', indexRouter)
      this.app.use('/orders', ordersRouter)  
    }
    private initDefaultData(){
        storage.init().then(() => {
        storage.setItem('orders', orders)})
        this.app.use(express.static(path.join(__dirname, 'public')))
        this.app.use(express.urlencoded({ extended: false }))
    }
}
