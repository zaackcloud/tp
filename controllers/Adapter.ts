import {Contact} from '../types';

export default class Adapteur implements Contact{
   public fullname: string;
   public email: string;
   public phone: string;
   public address: string;
   public postalCode: string;
   public city: string;

    constructor(){
        this.fullname = 'Frodo Baggins'
        this.email = 'preciousRKT@bagend.sh'
        this.phone = '555-0123456'
        this.address = '21 chemin des tallus bagend'
        this.postalCode= '59000'
        this.city = 'Hobbitton'
    }
}