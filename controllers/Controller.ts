import { Router, Request, Response } from 'express'
import Storage from '../models/storage'
import Order from '../types'
import Adapter from './Adapter'

export default class Controller{
    private storage : Storage;
    constructor() {
         this.storage = new Storage("orders");
    }

    public async GetOrders(req: Request, res: Response):Promise<void>{
        const orders:Order[] = await this.storage.getItems();
        orders.map(order => {
            order.delivery.contact = new Adapter()
        })
        res.json(orders);
    }
    public async GetOneOrders(req: Request, res: Response):Promise<void>{
        const id = req.params.id
        const orders = await this.storage.getItems()
        const result = orders.find((order: any) => order.id ===parseInt(id,10))
        if (!result) {
           res.sendStatus(404)
        }
         res.json(result)
    }
    public async AddOrders(req: Request, res: Response):Promise<void>{
        const orders = await this.storage.getItems()
        const alreadyExists =  orders.find((order: any) => order.id === req.body.id);
        if (alreadyExists) {
           res.sendStatus(409)
        }
        orders.push(req.body)
        await this.storage.setItems(orders)
        res.sendStatus(201)
    }
}