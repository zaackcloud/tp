#!/usr/bin/env node

import App from './app'
import server from './server'

const port = 8000;
const expApp = new App()

const serv = new server(port, expApp.getApp())

serv.start();