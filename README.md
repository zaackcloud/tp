## Question 2 ##

le patern d'architecture que nous souhaitons mettre en place ici est le MVC.
Ce pattern nous permet:

-   De clarifier la conception en séparant la vue, les modèles et le contrôleur du code, et permet ainsi de pouvoir modifier chaque partie indépendamment.
-   Cette indépendance des partie permet la séparation du travail entre plusieurs développeurs (Back/middle/front)
________________________________________________________________________________
## Question 3 ##

Le principe du SOLID utilisé dans cette question est le I. C'est à dire la ségrégation des interfaces. Préférer plusieurs interfaces spécifiques pour chaque client plutôt qu'une seule interface générale
________________________________________________________________________________
## Question 4 ##

Le Design Patter utilisé dans cette question est l'Adaptateur, celui-ci permet de modifier avec un simple fichier les infos du JSON
________________________________________________________________________________
## Question 5 ##

Nous avons utilisé le desing patern de la façade dans cette question, pour avoir accès qu'a une seule instance de l'application.
